
public class Persona {
    //             Atributos
    private String nombre;
    private String apellido;
    private int edad;
    private String sexo;
    public boolean voto;

    //              Metodos
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    public void verificarEdad(){
        if(this.edad>=18){
            voto = true;
        }else{
            voto = false;
        } 
    }
    
    public String eleccion(){
        if(this.voto==true){
            return "Puede votar";
        }else{
            return "No puede votar";
        }
    }
    
    //                  Construtor
    
    public Persona(String nombre, String apellido){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = 0;
    }
}
