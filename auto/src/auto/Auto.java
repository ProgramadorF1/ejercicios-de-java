package auto;

//Programacion orientada a objetos.

/*
Esto significa que pasaremos lo que esta en la realidad a programacion.
* Se basa en Atributos = Caracteristicas
* Metodos = Acciones
* Constructor = Sirve para inicializar los atributos
* Para cambiar el valor de los atributos es necesario el setter
  y para tomar se necesita el getter
* Tener en claro dos palabras get/set
---- set = Poner, colocar.      (Son muy importantes)
Sirve para colocar valores a mis atributos
---- get = Sacar, tomar, coger.  (Son muy importantes)
Sirve para sacar valores que tienen mis atributos.
* Puede tener getters o setters unicamente en cualquiera de los atributos 
  pero no es obligatorio.
* Se llaman normalmente getters y setters en java.
---- Void = vacio.. (No retornara nada)
---- This = esto o esta.
* Set va con el void \ y con un this.
* Get va con el return.
*/

public class Auto {

//Atributos de la clase auto
    private String Tamano;
    private int numPuertas;
    private int numLlantas;
    private String motor;
    private String marca;
    
//Metodos de la clase auto
    public String acelerar(){
        return "El auto se está moviendo";        
    }
    
    public String frenar(){
        return "El vehiculo se esta deteniendo";
    }
    
    public String reversa(){
        return "El vehiculo esta dando reversa";
    }
    
    public void setnumPuertas(int P){
        //(int P) = Parametro
        this.numPuertas = P;
    }
    
    public int getnumPuertas(){
        return numPuertas;
    }
    
    public void setmarca(String m){
        this.marca = m;
    }
    
    public String getmarca(){
        return marca;
    }
    
    public void setnumLlantas(int n){
        this.numLlantas = n;
    }
    
    public int getnumLlantas(){
        return numLlantas;
    }
    
    public void setTamano(String t){
        this.Tamano = t;
    }
    
    public String getTamano(){
        return Tamano;
    }
    
    public void setmotor(String M){
        this.motor = M;
    }
    
    public String getmotor(){
        return motor;
    }
    
//Constructor de la clase auto
    public Auto(){
        Tamano = "mediano";
        numPuertas = 4;
        numLlantas = 4;
        motor = "gasolina";
        marca = "mazda";
    }
    public static void main(String[] args) {
    /*  Clase es (Auto) \n (miCarrio) es el objeto \n (Auto()) es el constructor
        *Creando objeto de la clase Auto */
        Auto miCarro = new Auto();       
        System.out.println(miCarro.acelerar());
        
        System.out.println(miCarro.frenar());
        
        System.out.println(miCarro.reversa());
        
        System.out.println("El carro tiene "+ miCarro.getnumPuertas()+" Puertas");
        miCarro.setnumPuertas(6);
        System.out.println("El carro tiene "+ miCarro.getnumPuertas() +" Puertas");
        
        System.out.println("La marca del carro es "+ miCarro.getmarca());
        
        miCarro.setmarca("Chevrolet");        
        System.out.println("La marca del carro es "+ miCarro.getmarca());
        
        System.out.println("El tipo de motor es "+ miCarro.getmotor());
        
        miCarro.setmotor("Diesel");
        System.out.println("El tipo de motor es "+ miCarro.getmotor());
        
        System.out.println("El carro tiene "+ miCarro.getnumLlantas()+ " Llantas");
        
        miCarro.setnumLlantas(12);
        System.out.println("El carro tiene "+ miCarro.getnumLlantas() +" Llantas");
        
        System.out.println("El tamaño del Auto es "+ miCarro.getTamano());
        
        miCarro.setTamano("Pequeño o Familiar");
        System.out.println("El tamaño del Auto es "+ miCarro.getTamano());
    }
}
